[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

**We're BACK on RELEASE schedule!**

The first update in a very long time.

There is more to come if you want to take part in the pre-tests come to my discord and subchannel for this mod. [https://discord.gg/rx7D45x](https://discord.gg/rx7D45x)

The changes so far:

*   Actual volcano cones
*   in world smelting of full blocks to another when in contact with lava, we're referencing the furnace recipes correctly this time so it should not be causing massive stack dupes in a regular furnace any more.
*   default 5% chance to spawn
*   /spawnvolcano (for ops to test) (could be a spell later on)
*   Lava core now comprised of magma blocks
*   no longer checks all lava blocks
*   configurable ore for the cones
*   calderas
*   pools of lava in the calderas
*   spawn in the center of already generated chunks
*   can be configured to only spawn during worldgen and not when a player walks by
*   will spawn in range of a player configurable default 32 blocks
*   random height
*   random width
*   generates a pool of lava at y=10 configurable
*   all hard numbers are configurable
*   a tile entity (sign, chest, piston, furnace, machine from a mod will prevent spawning of volcano in a chunk unless summoned)
*   a check for spawning is done per chunk, if a chunk fails to spawn a volcano it is marked as been checked and will never be allowed to spawn a volcano unless one is summoned there. if a chunk passes the check for spawn it is marked as allowing a volcano to spawn and will eventually if the player moves in front of it enough. This latter part is moot if worldgen only is enabled.

Upcoming features:

*   underwater volcanos
*   magma vents
*   ash deposits
*   ash clouds (block out sun)
*   different volcano generation by biome type

Notes: We will likely port to 1.13.2 and 1.14.2 before adding the upcoming features except for the different volcano types.

![](https://cdn.discordapp.com/attachments/588698957723598848/591071481547718666/unknown.png)

**DANGER ADDING THIS TO YOUR WORLD CAN CAUSE DESTRUCTION LIKE YOU'VE NEVER SEEN BEFORE with the DEFAULT SETTINGS.
**

**Feel free to add this mod to existing worlds both single player and multiplayer. I have code in it that blocks Volcano formation in any chunk where a tileEntity already exists. Signs, chests, furnaces, and more are tileEntities and common in player builds.

REMEMBER TO BACKUP FIRST THO!**

## [![](https://media.forgecdn.net/attachments/318/929/dependency.png)](https://www.curseforge.com/minecraft/mc-mods/eleklib)

## Modpacks

---

Don't ask just add. A link back to this page would be appreciated but not required.

## Youtube - Twitch - Etc

---

Go ahead and please share the link so I can feature it.

## Help a Veteran today

I am Veteran of United States Army. I am not legally disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [ ![](https://i.imgur.com/WhT4CbN.png) ](https://patreon.com/kreezxil).
