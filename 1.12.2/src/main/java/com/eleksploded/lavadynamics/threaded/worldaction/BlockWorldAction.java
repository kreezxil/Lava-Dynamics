package com.eleksploded.lavadynamics.threaded.worldaction;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockWorldAction implements IWorldAction {
	
	private final IBlockState state;
	private final BlockPos pos;
	
	public BlockWorldAction(IBlockState state, BlockPos pos) {
		this.state = state;
		this.pos = pos;
	}

	public BlockWorldAction(BlockPos pos, IBlockState state) {
		this.state = state;
		this.pos = pos;
	}

	@Override
	public void takeAction(World world) {
		world.setBlockState(pos, state);
	}
	
	public BlockPos getBlockPos() {
		return pos;
	}
	
	public IBlockState getState() {
		return state;
	}

}
