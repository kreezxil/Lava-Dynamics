package com.eleksploded.lavadynamics.threaded.worldaction;

import net.minecraft.world.World;

public interface IWorldAction {
	public void takeAction(World world);
}
