package com.eleksploded.lavadynamics.threaded;

import java.util.Deque;
import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;

import com.eleksploded.lavadynamics.LavaConfig;
import com.eleksploded.lavadynamics.LavaDynamics;
import com.eleksploded.lavadynamics.Volcano;
import com.eleksploded.lavadynamics.storage.CheckedCap;
import com.eleksploded.lavadynamics.threaded.worldaction.BlockWorldAction;
import com.eleksploded.lavadynamics.threaded.worldaction.ExplosionWorldAction;
import com.eleksploded.lavadynamics.threaded.worldaction.IWorldAction;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class AsyncBlockPlacer {
	public static Deque<IWorldAction> actions = new LinkedBlockingDeque<>();

	public static void queueAction(IWorldAction action) {
		actions.add(action);
	}
	
	public static IBlockState queryForBlock(World world, BlockPos pos) {
		if(!actions.isEmpty()) {
			List<IWorldAction> inPos = actions.parallelStream().filter(a -> a instanceof BlockWorldAction).filter(a -> ((BlockWorldAction)a).getBlockPos().equals(pos)).collect(Collectors.toList());
		
			if(!inPos.isEmpty()) {
				return ((BlockWorldAction) inPos.get(inPos.size() - 1)).getState();
			}
		}
		return world.getBlockState(pos);
	}
	
	@SubscribeEvent
	public static void worldTick(TickEvent.WorldTickEvent e) {
		if(!actions.isEmpty()) {
			int iterations = Math.min(actions.size(), LavaConfig.threadedOptions.actionsPerTick);
			for(int i = 0; i < iterations; i++) {
				actions.poll().takeAction(e.world);
			}
		}
	}

	public static void queueVolcano(Chunk chunk, World world) {
		chunk.getCapability(CheckedCap.checkedCap, null).check();
		//----------Setup----------// 
		if(world.isRemote) { return; }
		boolean debug = LavaConfig.general.genVolcanoDebug;
		Random rand = new Random(world.getSeed());
		//Get the center of the chunk
		int x = (chunk.getPos().getXEnd() - chunk.getPos().getXStart())/2 + chunk.getPos().getXStart();
		int z = (chunk.getPos().getZEnd() - chunk.getPos().getZStart())/2 + chunk.getPos().getZStart();		
		//Get random y level based on LavaConfig value
		int y = LavaConfig.volcano.volcanoYLevel + (rand.nextInt(5) - 2);
		//Get center of chunk into BlockPos
		BlockPos center = new BlockPos(x,y,z);

		if(debug) {
			LavaDynamics.Logger.info("Center Location is " + center);
		}

		//----------Lava Lake----------//

		//Generate new Lava lake at chunk center
		WorldGenLakes lakes = new WorldGenLakes(Blocks.LAVA);
		lakes.generate(world, rand, center);
		if(debug) {
			LavaDynamics.Logger.info("Lava lake generated");
		}

		//----------Lava Pillar---------//

		if(debug) {
			LavaDynamics.Logger.info("Generating Lava Pillar");
		}

		//Get hight and set pillar
		int height = chunk.getHeight(new BlockPos(x,y+5,z)) - center.getY();
		if(height <= 0){
			height = 1;
		}
		if(debug){
			LavaDynamics.Logger.info("Height for Generation is: " + height);
		}

		//Save top value for later. Init with random value
		int topY = 70;

		for(int i = 0; i <= height; i++) {
			//Add starting height in. Cast int cause it was treating them as Strings for some reason
			int j = (int)i + (int)center.getY();
			if(debug) {
				LavaDynamics.Logger.info("Placing Lava at y=" + j);
			}
			//Set Block, which causes block updates to smelt things
			queueAction(new BlockWorldAction(new BlockPos(x,j,z), Blocks.MAGMA.getDefaultState()));
			//Save TopY
			topY = j;
		}
		if(debug) {
			LavaDynamics.Logger.info("Lava Pillar Done");
		}

		//----------Surface Generation-----------//
		if(debug) {
			LavaDynamics.Logger.info("Generate Volcano");
		}
		
		//Custom WorldGenerator
		WorldGenerator gen = Volcano.getGenerator(world, new BlockPos(x, 255, z));
		gen.generate(world, rand, new BlockPos(x, 255, z));
		if(debug) {
			LavaDynamics.Logger.info("Done Generating. Filling with " + Blocks.LAVA);
		}
		//Fill the "Volcano" with lava
		BlockPos fill = new BlockPos(x,topY,z);
		while(queryForBlock(world, fill).getBlock() != Blocks.LAVA || queryForBlock(world, fill).getBlock() != Blocks.AIR || queryForBlock(world, fill).getBlock() != Blocks.MAGMA) {
			if(debug) {
				LavaDynamics.Logger.info("Block at " + fill + " is " + queryForBlock(world, fill).getBlock());
				LavaDynamics.Logger.info("Setting " + fill + " to lava");
			}
			queueAction(new BlockWorldAction(fill, Blocks.LAVA.getDefaultState()));
			fill = fill.up();
			//Check for air
			if(queryForBlock(world, fill).getBlock() == Blocks.AIR){ break; }
			if(queryForBlock(world, fill).getBlock() == Blocks.WATER){ break; }
			//Cap at world limits, hopefully this should never happen though
			if(fill.getY() >= 255 || fill.getY() <= 3) {
				break;
			}
			chunk.getCapability(CheckedCap.checkedCap, null).setVolcano(fill.getY());
		}
		
		if(debug) {
			LavaDynamics.Logger.info("Done filling, Spawning crater");
		}
		
		BlockPos fill1 = fill.down(4);
		
		for(int radius = 3;radius != 0;radius--){
			int x1 = fill1.getX();
			int y1 = fill1.getY()-1;
			int z1 = fill1.getZ();
			
			for(float i1 = 0; i1 < radius; i1 += 0.5) {
				for(float j1 = 0; j1 < 2 * Math.PI * i1; j1 += 0.5)
					queueAction(new BlockWorldAction(new BlockPos((int)Math.floor(x1 + Math.sin(j1) * i1), y1 + radius, (int)Math.floor(z1 + Math.cos(j1) * i1)), Blocks.LAVA.getDefaultState()));
			
			}
			fill1 = fill1.down();
		}

		//Make our "eruption"
		queueAction(new ExplosionWorldAction(null, fill.getX(), fill.getY()+3, fill.getZ(), LavaConfig.volcano.craterSize, LavaConfig.volcano.initialFire, true));
		
		if(debug) {
			LavaDynamics.Logger.info("Done with crater");
		}
	}
}
